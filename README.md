Wikibase reconciliation endpoint for OpenRefine 
================================================

This repository is a fork from [https://github.com/wetneb/openrefine-wikibase](https://github.com/wetneb/openrefine-wikibase).   

The endpoint is described in the paper ["Running a reconciliation service for Wikidata", Antonin Delpeuch](http://ceur-ws.org/Vol-2773/paper-17.pdf).  

## Requirements

Deploying this instance as described below requires [Docker](https://www.docker.com/) (tested with v20.10.17) and [Docker Compose](https://docs.docker.com/compose/) (tested with v2.9.0).

## Configuration

Update the [`config.sh`](config.sh) file to feature infromation pertaining to your specific Wikibase instance.

Expected variables are:
```sh
wikibase_name= # the name of your wikibase

wikibase_address= # the web address of your wikibase instance (without https://)

query_address= # the web address of your query service (without https://)

reconcile_address="http://localhost:8000" # this can remain unchanged
```

Once the config file has been updated it can be run via the following command from the terminal:

```sh
./config.sh
```

This will generate two config files, `config.py` and `manifest.json`

## Launch endpoint

To run the service run the following command:
```sh
docker compose up -d
```

If there are no problems encountered, the service page should now be visible at [http://localhost:8000](http://localhost:8000).

## Using the endpoint

Download OpenRefine (3.7 developer snapshot recommended) from [here](https://github.com/OpenRefine/OpenRefine/releases/tag/3.7-beta2).

Run OpenRefine using the following command from within the project directory:

```sh
./refine
```

OpenRefine should be available at [http://localhost:3333](http://localhost:3333)

Select the data file which you wish to upload (`test.csv` in this instance).

Select `Next >>`

![Python tests](/screenshots/screenshot1.png)

Select `Create project >>`

![Python tests](/screenshots/screenshot2.png)

Under `Extensions` select `Manage Wikibase instances...`

![Python tests](/screenshots/screenshot3.png)

Select `Add Wikibase`

![Python tests](/screenshots/screenshot4.png)

Add `http://localhost:8000/static/manifest.json` into the appropriate field and `Add Wikibase`

![Python tests](/screenshots/screenshot5.png)

Select your Wikibase and `OK`

![Python tests](/screenshots/screenshot6.png)

Select a column to `reconcile` and `Start reconciling...`

![Python tests](/screenshots/screenshot7.png)

Select `Add standard service...`     
Add `http://localhost:8000/en/api` into the appropriate field and `Add Service`

![Python tests](/screenshots/screenshot8.png)

Select `Start reconciling...` to begin reconciling data against Wikibase

![Python tests](/screenshots/screenshot9.png)

~

MIT license.
